module Graphviz.Parser

import Lightyear.Core
import Lightyear.Strings
import Lightyear.Char
import Lightyear.Combinators
import Graphviz

nelP : Parser t -> Parser $ NEL t
nelP tp = (some tp) >>= listToNel
  where 
    listToNel : (Monad m, Alternative m) => List t -> m $ NEL t
    listToNel [] = empty
    listToNel (x :: xs) = pure $ MkDPair (x :: xs) IsNonEmpty
    
--scientific stuf from idris-config/Utils  
record Scientific where
  constructor MkScientific
  coefficient : Integer
  exponent : Integer

scientificToFloat : Scientific -> Double
scientificToFloat (MkScientific c e) = fromInteger c * exp
  where
    exp = if e < 0
            then 1 / pow 10 (fromIntegerNat (- e))
            else pow 10 (fromIntegerNat e)

parseScientific : Parser Scientific
parseScientific = do
    sign <- maybe 1 (const (-1)) `map` opt (char '-')
    digits <- some digit
    hasComma <- isJust `map` opt (char '.')
    decimals <- if hasComma
                then some digit
                else pure Prelude.List.Nil
    hasExponent <- isJust `map` opt (char 'e')
    exponent <- if hasExponent
                then integer
                else pure 0
    pure $ MkScientific (sign * fromDigits (digits ++ decimals)) (exponent - cast (length decimals))
  where
    fromDigits : List (Fin 10) -> Integer
    fromDigits = foldl (\a, b => 10 * a + cast b) 0
--

double : Parser Double
double = scientificToFloat <$> parseScientific

literal : String -> a -> Parser a
literal name obj = token name *> pure obj

either : Parser a -> Parser b -> Parser $ Either a b
either ap bp = Left <$> ap <|> Right <$> bp
    
compassP : Parser Compass
compassP = token ":" *> (literal "n" N 
  <|> literal "ne" NE 
  <|> literal "e" E 
  <|> literal "se" SE 
  <|> literal "s" S 
  <|> literal "sw" SW 
  <|> literal "w" W 
  <|> literal "nw" NW 
  <|> literal "c" C 
  <|> literal "_" Underscore)

edgeOpP : (gt: GraphType) -> Parser $ EdgeOp gt
edgeOpP Directed = literal "->" DiEdge
edgeOpP Undirected = literal "--" UndirEdge

idP : Parser ID
idP = [| StringID (pack <$> some alphaNum) |] <|> [| NumeralID double |] <|> [| StringLiteralID (quoted '\"') |] <|> [| HTMLString (quoted' '<' '>') |]

portP : Parser Port
portP = token ":" *> [| MkPort (opt idP) compassP |]

nodeIdP : Parser NodeId
nodeIdP = [| MkNodeId idP (opt portP) |]

aListP : Parser AList
aListP = sepBy1 [| MkPair idP (token "=" *> idP) |] (opt $ either semi comma)

attrListP : Parser AttrList
attrListP = many $ brackets aListP

nodeStmtP : Parser NodeStmt
nodeStmtP = [| MkNodeStmt nodeIdP attrListP |]

attrStmtP : Parser AttrStmt
attrStmtP = (token "graph" <|> token "node" <|> token "edge") *> [| MkAttrStmt attrListP |]

mutual
  stmtP : (gt: GraphType) -> Parser $ Stmt gt
  stmtP gt = [| Node nodeStmtP |]
    <|> [| Edge (edgeStmtP gt) |]
    <|> [| Attr attrStmtP |]
    <|> [| IdEq idP (token "=" *> idP) |]
    <|> [| Sub (subgraphP gt) |]
  
  edgeStmtP : (gt: GraphType) -> Parser $ EdgeStmt gt
  edgeStmtP gt = sepBy1 (either nodeIdP (subgraphP gt)) (edgeOpP gt) >>= listToNel
    where 
      listToNel : (Monad m, Alternative m) => List $ t -> m $ NEL t
      listToNel [] = empty
      listToNel (x :: xs) = pure $ MkDPair (x :: xs) IsNonEmpty
  
  subgraphP : (gt: GraphType) -> Parser $ Subgraph gt
  subgraphP gt = (opt $ token "subgraph") *> [| MkSubgraph (opt idP) (braces $ semiSep $ stmtP gt) |]
  
export graphP : Parser Graph
graphP = (opt $ token "strict") *>
  ((token "digraph" >! [| DiGraph (opt idP) (braces $ many $ stmtP Directed) |]) 
  <|> (token "graph" >! [| UndiGraph (opt idP) (braces $ many $ stmtP Undirected) |]))
