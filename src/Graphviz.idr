module Graphviz

%access public export

NEL : Type -> Type
NEL ty = (l: List ty ** NonEmpty l)

data Compass = N | NE| E | SE | S | SW | W | NW | C | Underscore
data GraphType = Directed | Undirected

data EdgeOp : GraphType -> Type where
  DiEdge : EdgeOp Directed
  UndirEdge : EdgeOp Undirected

data ID = StringID String
        | NumeralID Double
        | StringLiteralID String
        | HTMLString String

record Port where
  constructor MkPort
  id : (Maybe ID)
  compass : Compass

record NodeId where
  constructor MkNodeId
  id : ID 
  port : (Maybe Port)

AList : Type
AList = List $ (ID, ID)

AttrList : Type
AttrList = List $ AList

record NodeStmt where
  constructor MkNodeStmt
  id : NodeId
  attrs : AttrList
  
record AttrStmt where
  constructor MkAttrStmt
  attrs : AttrList
  
mutual
  data Stmt : GraphType -> Type where
    Node : NodeStmt -> Stmt gt
    Edge : EdgeStmt gt -> Stmt gt
    Attr : AttrStmt -> Stmt gt
    IdEq : ID -> ID -> Stmt gt
    Sub : Subgraph gt -> Stmt gt
     
  EdgeStmt : GraphType -> Type
  EdgeStmt gt = NEL $ Either NodeId $ Subgraph gt
    
  data Subgraph : GraphType -> Type where MkSubgraph : Maybe ID -> List $ Stmt gt -> Subgraph gt
    
data Graph : Type where
  DiGraph : Maybe ID -> List $ Stmt Directed -> Graph
  UndiGraph : Maybe ID -> List $ Stmt Undirected -> Graph
